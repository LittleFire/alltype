#include "alltypes.hpp"
#include <cstring>

/// Structure definitions

struct Spair {
    size_t num, // Number of elements
           sze; // Size of each
};

struct Node {
    void*  ptr; // location
    Spair  sze; // size
    size_t typ; // type
};

struct AllType {
    Node** ary; // values
    size_t len; // length
    bool   err, // error
           gdf; // good
};

struct Svar {
    Node nde; // value
};

struct Heap {
    void*  loc; // Location
    size_t hst, // Host number
           ttl, // Total
           usd; // Used
};

struct Host {
    void*  ptr; // Location
    size_t sze; // Size of
};

struct Capture {
    Host hst; // value
};

inline bool is_error(const Erno input) // Check for NO_ERROR.
{ return input != Erno::NO_ERROR; }

#define ARYD(x) (x).num>1
#define ARXD(x) (x).sze>1
#define ARDD(x) ARYD((x))? (x).num:1
#define ARSD(x) ARXD((x))? (x).sze:1
#define ARRD(x) (ARYD((x))? ARDD((x))*ARSD((x)):ARSD((x)))

#define error return Erno::
#define noerror return Erno::NO_ERROR

/// Unmarked pointer functions

namespace {

    inline bool _PTR_eq(void** ptr1, void** ptr2) {
        char* pnt1 = (char*)ptr1; char* pnt2 = (char*)ptr2;
        for(size_t i=0; i<(size_t)sizeof(void*); i++) {
            if((*(pnt1+i)) != (*(pnt2+i))) {
                return false;
            }
        } return true;
    }

    inline bool _PTR_null(void** ptr) {
        char* pnt = (char*)ptr;
        for(size_t i=0; i<(size_t)sizeof(void*); i++) {
            if((*(pnt+i)) != '\0') {
                return false;
            }
        } return true;
    }

    /// Memory

        inline void* __ReAlloc(void* src, size_t x, size_t y) {
            if (_PTR_null(&src)) {return  calloc(     x,y);}
                             else{return realloc(src, x*y);}
        }

    inline void* _PTR_render(void** ptr) {
        void* dest = NULL;
        for(size_t i=0; i<(size_t)sizeof(void*); i++) {
            memcpy(((char*)&dest)+i, ((char*)ptr)+i, sizeof(char));
        } return dest;
    }

    /// Arithmetic

    inline void** _PTR_arithmetic(void** ptr, size_t cnt) {
        return (void**) (((char*)ptr) + ((size_t)sizeof(void*) * cnt));
    }

    inline void*  _PTR_arithmetic_rendered(void** ptr, size_t cnt) {
        return _PTR_render(_PTR_arithmetic(ptr, cnt));
    }

    inline void*  _PTR_counting  (void*  ptr, size_t cnt) {
        return (void* ) (((char*)ptr) + ((size_t)sizeof(void*) * cnt));
    }

    inline size_t _PTR_subtact(void*  srt, void*  end) {
        return (size_t) end - (size_t) srt;
    }

    inline size_t _PTR_subtact(void** srt, void** end) {
        return _PTR_subtact(_PTR_render(srt), _PTR_render(end));
    }

}

namespace alt {

    namespace {

        /// Unmarked functions

        inline Node* _newNode_free(const Coord sz) {
            Node* base = (Node*) malloc(sizeof(Node));
            if(/*!_PTR_null((void**)&base)*/ base) {
                void* c = calloc(sz->num, sz->sze);
                if(/*!_PTR_null(&c)*/ c) {
                    base->ptr = c;
                    base->sze.num = sz->num;
                    base->sze.sze = sz->sze;
                    base->typ = 0;
                } else {
                    free(base);
                    base = NULL;
                }
            } return base;
        }

        inline Erno _newNode_error(const Coord sz) {
            if (blankSpair(sz)) {error NULL_OBJECT;}
            noerror;
        }

        inline Node* _copyNode_free(const Node* obj) {
            Node* ptr = _newNode_free((const Coord) &(obj->sze));
            if(/*!_PTR_null((void**)&ptr)*/ ptr) {
                memcpy(ptr->ptr, obj->ptr, ARRD(obj->sze));
                ptr->typ = obj->typ;
                ptr->sze.num = obj->sze.num;
                ptr->sze.sze = obj->sze.sze;
            } return ptr;
        }

        inline Erno _copyNode_error(const Node* obj) {
            if(/*_PTR_null((void**)&obj)*/ !obj) {error NULL_OBJECT;}
            noerror;
        }

        inline void _deleteNode_free(Node** obj, const bool nullout) {
            const Node* nptr = (Node*) _PTR_render((void**)&obj);
            free((nptr)-> ptr );
            free((void*)(nptr));
            if(nullout) {
                /*memset((void*)obj, 0, sizeof(Node*));*/
                *obj = NULL;
            }
        }

        inline Erno _deleteNode_error(const Node** obj) {
            if(/*_PTR_null((void**)&obj)*/ ! obj) {error NULL_OBJECT  ;}
            if(/*_PTR_null((void**) obj)*/ !*obj) {error NULL_ARGUMENT;}
            noerror;
        }

        inline bool _imposeNode_free(Node* from, Node* to) {
            if(/*_PTR_null((void**)&from)*/ !from) {
                _deleteNode_free(&to, false);
                return true;
            }
            void* ral = __ReAlloc(to->ptr, ARDD(from->sze), ARSD(from->sze));
            if(/*!_PTR_null((void**)&ral)*/ ral) {
                memcpy(ral, from->ptr, ARYD(from->sze));
                to->ptr = ral;
                to->sze.num = from->sze.num;
                to->sze.sze = from->sze.sze;
                to->typ = from->typ;
                return true;
            }
            _deleteNode_free(&to, false);
            return false;
        }

        inline Erno _imposeNode_error(const Node* from, const Node* to) {
            if(/*_PTR_null((void**)&from)*/ !from) {error NULL_OBJECT  ;}
            if(/*_PTR_null((void**)&to  )*/ !to  ) {error NULL_ARGUMENT;}
            noerror;
        }

        /// Unmarked node functions

        inline bool __NewNode_free(Node* obj, const Coord sz) {
            if (blankSpair(sz)) {
                free(obj->ptr);
                return true;
            }
                obj->ptr = NULL;
                obj->sze.num = 0;
                obj->sze.sze = 0;
                obj->typ = 0;
            void* c = calloc(sz->num, sz->sze);
            if(/*!_PTR_null(&c)*/ c) {
                obj->ptr = c;
                obj->sze.num = sz->num;
                obj->sze.sze = sz->sze;
                return true ;
            }   return false;
        }

        inline Erno __NewNode_error(const Node* obj, const Coord sz) {
            if(/*_PTR_null((void**)&obj)*/ !obj) {error NULL_OBJECT  ;}
            if(blankSpair(sz)                  ) {error NULL_ARGUMENT;}
            noerror;
        }

        inline bool __ImposeNode_free(Node* from, Node* to) {
            if(/*_PTR_null((void**)&(from->ptr))*/ !(from->ptr)) {
                free(to->ptr) ;
                to->ptr = NULL;
                return true   ;
            }
            //
            void* ral = __ReAlloc(to->ptr, ARDD(from->sze), ARSD(from->sze));
            if(/*!_PTR_null((void**)&ral)*/ ral) {
                memcpy(ral, from->ptr, ARYD(from->sze));
                to->ptr = ral;
                to->sze.num = from->sze.num;
                to->sze.sze = from->sze.sze;
                to->typ = from->typ;
                return true;
            }
            free(to-> ptr);
            to->ptr = NULL;
            return false  ;
        }

        inline Erno __ImposeNode_error(Node* from, Node* to) {
            if(/*_PTR_null((void**)&from)*/ !from) {error NULL_OBJECT  ;}
            if(/*_PTR_null((void**)&to  )*/ !to  ) {error NULL_ARGUMENT;}
            if(/*_PTR_null((void**)&(from->ptr))*/ !(from->ptr)) {error NULL_OBJECT  ;}
            if(/*_PTR_null((void**)&(to  ->ptr))*/ !(to  ->ptr)) {error NULL_ARGUMENT;}
            noerror;
        }

        /// Miscellaneous

        inline void _Pave_free (Morph obj) {
            obj->ary = NULL ;
            obj->len = 0    ;
            obj->err = false;
            obj->gdf = false;
        }

        inline Erno _Pave_error (Morph obj) {
            if (/*_PTR_null((void**)&obj)*/ obj) {error NULL_OBJECT;}
            noerror;
        }

        inline void _Paver_free(Chest obj) {
            obj->nde.ptr = NULL;
            obj->nde.sze.num = 0;
            obj->nde.sze.sze = 0;
            obj->nde.typ = 0;
        }

        inline Erno _Paver_error(Chest obj) {
            if (/*_PTR_null((void**)&obj)*/ obj) {error NULL_OBJECT;}
            noerror;
        }

    }


    /// Sector A


    Morph create(size_t sz) // Create a container. (element_size)
    {
        if (is_error(create_error(sz))) { return NULL; }
        return create_free(sz);
    }

    Morph create_free(size_t sz)
    {
        Morph blk = blank();
        if (!recreate(blk, sz)) {
            destroy(blk, false);
            return NULL;
        }   return blk ;
    }

    Erno create_error(size_t sz) {
        if (sz == 0) {error ZERO_ARGUMENT;}
        noerror;
    }

    Morph create(Coord dim) // Create a container. (element_size)
    {
        if (is_error(create_error(dim))) { return NULL; }
        return create_free(dim);
    }

    Morph create_free(Coord dim) // Create a container. (element_size, number_of_elements)
    {
        Morph blk = blank();
        if (!recreate(blk, dim)) {
            destroy(blk, false);
            return NULL;
        }   return blk ;
    }

    Erno create_error(Coord dim) {
        if (blankSpair(dim)) {error NULL_ARGUMENT;}
        noerror;
    }

    bool recreate(Morph obj, size_t sz) // ReCreate a container. (element_size)
    {
        if (is_error(recreate_error(obj, sz))) { return false; }
        return recreate_free(obj, sz);
    }

    bool recreate_free(Morph obj, size_t sz)
    {
        Node** pntr = (Node**) calloc(1, sizeof(Node*));
        if (/*!_PTR_null((void**)&pntr)*/ pntr) {
            const Coord crd = createSpair(sz);
            Node* nde = _newNode_free(crd);
            free(crd);
            //
            if (/*!_PTR_null((void**)&nde)*/ nde) {
                obj->len = 1;
                memcpy((void*)pntr, &nde, sizeof(Node*));
                obj->ary = pntr;
            } else {
                obj->err = true ;
                obj->gdf = false;
                free(pntr)  ;
                return false;
            }
        } else {
            obj->err = true ;
            obj->gdf = false;
            return     false;
        }
        obj->err = false;
        obj->gdf = true ;
        return     true ;
    }

    Erno recreate_error(Morph obj, size_t sz)
    {
        if (/*_PTR_null((void**)&obj)*/ !obj) {error NULL_OBJECT  ;}
        if (                            !sz ) {error ZERO_ARGUMENT;}
        noerror;
    }

    bool recreate(Morph obj, Coord dim) // ReCreate a container. (element_size)
    {
        if (is_error(recreate_error(obj, dim))) { return false; }
        return recreate_free(obj, dim);
    }

    bool recreate_free(Morph obj, Coord dim) // ReCreate a container. (element_size, number_of_elements)
    {
        Node** ary = (Node**) calloc(dim->num, sizeof(Node*));
        if (/*!_PTR_null((void**)&ary)*/ ary) {
            Node* nde;
            obj->len = dim->num;
            const Coord crd = createSpair(dim->sze);
            for (size_t i = 0; i < (dim->num); i++) {
                nde = _newNode_free(crd);
                if (/*!_PTR_null((void**)&nde)*/ nde) {
                    memcpy((void*)_PTR_arithmetic((void**)ary, i), (void*)&nde, sizeof(Node*));
                } else {
                    obj->err = true ;
                    obj->gdf = false;
                    for (size_t j = 0; j < i; j++) {
                        _deleteNode((Node**)_PTR_arithmetic((void**)ary, j), false);
                    }
                    free(ary);
                    free(crd);
                    return false;
                }
            }
            free(crd);
            obj->ary = ary;
        } else {
            obj->err = true ;
            obj->gdf = false;
            return     false;
        }
        obj->err = false;
        obj->gdf = true ;
        return     true ;
    }

    Erno recreate_error(Morph obj, Coord dim)
    {
        if (/*_PTR_null((void**)&obj)*/ !obj) {error NULL_OBJECT  ;}
        if (blankSpair(dim)                 ) {error ZERO_ARGUMENT;}
        noerror;
    }

    void destroy(Morph obj, bool bl) {
        if (is_error(destroy_error(obj, bl))) { return; }
        destroy_free(obj, bl);
    }

    void destroy_free(Morph obj, bool bl) // UnCreate a container.
    {
        if (/*!_PTR_null((void**)&(obj->ary))*/ obj->ary) {
            for (size_t i = 0; i < (obj->len); i++) {
                _deleteNode((Node**)_PTR_arithmetic((void**)(obj->ary), i), false);
            }
            free(obj->ary);
        }
        if (bl) {_Pave_free(obj);}
        else    {      free(obj);}
    }

    Erno destroy_error(Morph obj, bool)
    {
        if (/*_PTR_null((void**)&obj)*/ !obj) {error NULL_OBJECT;}
        noerror;
    }

    bool copy_onto(Morph frm, Morph to)
    {
        if (is_error(copy_onto_error(frm, to))) { return false; }
        return copy_onto_free(frm, to);
    }

    bool copy_onto_free(Morph frm, Morph to) // Copy a onto b.
    {
        frm->err = false;
        to ->err = false;
        //
        const Coord crd = createSpair(frm->len, false);
        Morph tmp = create(crd);
        free(crd);
        //
        if (/*!_PTR_null((void**)&tmp)*/ tmp) {
            for (size_t i = 0; i < (frm->len); i++) {
                if (!_imposeNode((Node*)_PTR_arithmetic_rendered((void**)frm->ary, i),
                                 (Node*)_PTR_arithmetic_rendered((void**)tmp->ary, i))) {
                    destroy_free(tmp, false);
                    frm->err = true;
                    to ->err = true;
                    return false;
                }
            }
            destroy_free(to, true);
            to->ary = tmp->ary;
            to->len = tmp->len;
            to->gdf = tmp->gdf;
            free(tmp);
        } else {
            frm->err = true;
            to ->err = true;
            return false;
        }   return true ;
    }

    Erno copy_onto_error(Morph frm, Morph to)
    {
        if (!flag(frm, false)             ) {error ZERO_ARGUMENT;}
        if (!/*_PTR_null((void**)&to)*/ to) {error NULL_ARGUMENT;}
        noerror;
    }

    Morph make_copy(Morph obj)
    {
        if (is_error(make_copy_error(obj))) { return NULL; }
        return make_copy_free(obj);
    }

    Morph make_copy_free(Morph obj) // Copy a.
    {
        Morph blk = blank();
        if (copy_onto(obj, blk)) {return blk;}
        destroy(blk, false);
        return NULL;
    }

    Erno make_copy_error(Morph obj)
    {
        if (!flag(obj, false)) {error ZERO_ARGUMENT;}
        noerror;
    }

    bool flag(Morph obj, bool bl)
    {
        if (is_error(flag_error(obj, bl))) { return bl; }
        return flag_free(obj, bl);
    }

    bool flag_free(Morph obj, bool bl) // Retrieve the state of a flag in a container.
    {
        if (bl) {return obj->err;}
                 return obj->gdf;
    }

    Erno flag_error(Morph obj, bool)
    {
        if (/*_PTR_null((void**)&obj)*/ !obj) {error NULL_ARGUMENT;}
        noerror;
    }

    size_t length(Morph obj)
    {
        if (is_error(length_errors(obj))) { return 0; }
        return length_free(obj);
    }

    size_t length_free(Morph obj) // Retrieve the number of elements in a container.
    {
        return obj->len;
    }

    Erno length_errors(Morph obj)
    {
        if (/*_PTR_null((void**)&obj)*/ !obj) {error NULL_ARGUMENT;}
        noerror;
    }

    bool resizer(Morph obj, size_t sz)
    {
        if (is_error(resizer_error(obj, sz))) { return false; }
        return resizer_free(obj, sz);
    }

    bool resizer_free(Morph obj, size_t sz) // Change the number of elements in a container.
    {
        obj->err = false;
        if (obj->len == sz) {return true;}
            Node** ptr0;
            #define ptr0 tpr
        if (obj->len <  sz) {
            const size_t diff = sz - obj->len;
            Node** tmp = (Node**) calloc(diff, sizeof(Node*));
            if (/*!_PTR_null((void**)&tmp)*/ tmp) {
                Node* ptr;
                const Coord crd = createSpair(1);
                for (size_t i = 0; i < diff; i++) {
                    ptr = _newNode_free(crd);
                    if (/*_PTR_null((void**)&ptr)*/ ptr) {
                        obj->err = true;
                        for (size_t j = 0; j < i; j++) {
                            tpr = (Node**) _PTR_arithmetic((void**)tmp, j);
                            if (is_error(_deleteNode_error(tpr))) { continue; }
                            _deleteNode_free(tpr, false);
                        }
                        free(tmp);
                        free(crd);
                        return false;
                    }
                    memcpy((void*)_PTR_arithmetic((void**)tmp, i), (void*)&ptr, sizeof(Node*));
                }
                free(crd);
                ptr0 = (Node**) __ReAlloc(obj->ary, sz, sizeof(Node*)); // WARNING
                if (/*!_PTR_null((void**)&ptr0)*/ ptr0) {
                    for (size_t i = obj->len; i < sz; i++) {
                        memcpy((void*)_PTR_arithmetic((void**)ptr0, i           ),
                               (void*)_PTR_arithmetic((void**)tmp , i-(obj->len)),
                               sizeof(Node*));
                    } // WARNING
                    obj->ary = ptr0;
                    obj->len = sz  ;
                    free(tmp)  ;
                    return true;
                }
                for (size_t j = 0; j < diff; j++) {
                    tpr = (Node**) _PTR_arithmetic((void**)tmp, j);
                    if (is_error(_deleteNode_error(tpr))) { continue; }
                    _deleteNode_free(tpr, false);
                }
                free(tmp);
                obj->err = true;
            }
            return false;
        } else {
            Node** tpr;
            for (size_t i = (obj->len - 1); i >= sz; i--) {
                tpr = (Node**)_PTR_arithmetic((void**)(obj->ary), i);
                if (is_error(_deleteNode_error((const Node**)tpr))) { continue; }
                _deleteNode_free(tpr, false);
            } // WARNING
            Node** ptr = (Node**) __ReAlloc(obj->ary, sz, sizeof(Node*)); // WARNING
            if (/*!_PTR_null((void**)&ptr)*/ ptr) {
                obj->ary = ptr;
                obj->len = sz ;
                return true   ;
            }
            obj->err = true;
            return false   ;
        }
    }

    Erno resizer_error(Morph obj, size_t sz)
    {
        if (!flag(obj, false)) { error NULL_OBJECT  ; }
        if (sz == 0          ) { error ZERO_ARGUMENT; }
        noerror;
    }

    bool inserter(Morph obj, size_t ind)
    {
        if (is_error(inserter_error(obj, ind))) { return false; }
        return inserter_free(obj, ind);
    }

    bool inserter_free(Morph obj, size_t ind) // Add an element to a container at an index.
    {
        const size_t lnt = length_free(obj);
        if (resizer_free(obj, lnt + 1)) {
            Node* nde = (Node*) _PTR_arithmetic_rendered((void**)(obj->ary), lnt); // WARNING
            for (size_t i = lnt; i > ind; i--) { // suspend by 1
                memcpy((void*)_PTR_arithmetic((void**)(obj->ary), i  ), (void*)_PTR_arithmetic((void**)(obj->ary), i-1), sizeof(Node*)); // un-suspend
            }   memcpy((void*)_PTR_arithmetic((void**)(obj->ary), ind), (void*)&nde                                    , sizeof(Node*));
            return true ;
        }   return false;
    }

    Erno inserter_error(Morph obj, size_t ind)
    {
        if (length(obj) <= ind    ) {error ARGUMENT_OUT_OF_BOUNDS;}
        if (!flag_free(obj, false)) {error NULL_OBJECT           ;}
        noerror;
    }

    bool deleter(Morph obj, size_t ind)
    {
        if (is_error(deleter_error(obj, ind))) { return false; }
        return deleter_free(obj, ind);
    }

    bool deleter_free(Morph obj, size_t ind) // Remove an element from a container at an index.
    {
        const size_t lnt = length_free(obj) - 1;
        Node* nde = NULL;
        Node** tpr = (Node**) _PTR_arithmetic((void**)(obj->ary), ind);
        if (!is_error(_deleteNode_error((const Node**)tpr))) {_deleteNode_free(tpr, false);}
        for (size_t i=ind + 1; i <= lnt; i++) {
            memcpy((void*)_PTR_arithmetic((void**)(obj->ary), i-1), (void*)_PTR_arithmetic((void**)(obj->ary), i), sizeof(Node*));
        }   memcpy((void*)_PTR_arithmetic((void**)(obj->ary), lnt), (void*)&nde                                  , sizeof(Node*));
        return resizer_free(obj, lnt);
    }

    Erno deleter_error(Morph obj, size_t ind)
    {
        const size_t lnt = length(obj);
        if (lnt             <= 1  ) {error ARGUMENT_TOO_LOW ;}
        if (lnt             <= ind) {error ARGUMENT_TOO_HIGH;}
        if (!flag_free(obj, false)) {error NULL_OBJECT      ;}
        noerror;
    }

    bool is_index_null(Morph obj, size_t ind)
    {
        if (is_error(is_index_null_error(obj, ind))) { return true; }
        return is_index_null_free(obj, ind);
    }

    bool is_index_null_free(Morph obj, size_t ind) // Check if an index of a container is ready for use.
    {
        return _PTR_arithmetic_rendered((void**)&(obj->ary), ind) == NULL;
    }

    Erno is_index_null_error(Morph obj, size_t ind)
    {
        if (length(obj) <= ind) {error ARGUMENT_OUT_OF_BOUNDS;}
        noerror;
    }

    size_t typer(Morph obj, size_t ind)
    {
        if (is_error(typer_error(obj, ind))) { return 0; }
        return typer_free(obj, ind);
    }

    size_t typer_free(Morph obj, size_t ind) // Retrieve the type of an element in a container.
    {
        if (_PTR_null(_PTR_arithmetic((void**)(obj->ary), ind))) {return 0;} // Keep.
        return ((Node*)(_PTR_arithmetic_rendered((void**)(obj->ary), ind)))->typ;
    }

    Erno typer_error(Morph obj, size_t ind)
    {
        if (length(obj) <= ind    ) {error ARGUMENT_OUT_OF_BOUNDS;}
        if (!flag_free(obj, false)) {error NULL_OBJECT           ;}
        noerror;
    }

    size_t sizes(Morph obj, size_t ind, bool rad)
    {
        if (is_error(sizes_error(obj, ind, rad))) {return 0;}
        return sizes_free(obj, ind, rad);
    }

    size_t sizes_free(Morph obj, size_t ind, bool rad) // Retrieve the size of an element in a container.
    {
        if (_PTR_null(_PTR_arithmetic((void**)(obj->ary), ind))) {return 0;}
        return readSpair( (Coord)&( ((Node*)_PTR_arithmetic_rendered((void**)(obj->ary), ind))->sze ), rad);
    }

    Erno sizes_error(Morph obj, size_t ind, bool)
    {
        if (length(obj) <= ind    ) {error ARGUMENT_OUT_OF_BOUNDS;}
        if (!flag_free(obj, false)) {error NULL_OBJECT           ;}
        noerror;
    }

    bool resizer(Morph obj, size_t ind, size_t sz)
    {
        if (is_error(resizer_error(obj, ind, sz))) { return false; }
        return resizer_free(obj, ind, sz);
    }

    bool resizer_free(Morph obj, size_t ind, size_t sz) // Change the size of an element in a container.
    {
        if (_PTR_null(_PTR_arithmetic((void**)(obj->ary), ind))) {return false;}
        Node* nde = (Node*) (_PTR_arithmetic_rendered((void**)(obj->ary), ind));
        if (sz == 0) {
            _deleteNode(&nde, true);
            return true;
        }
        const size_t num = ARDD(nde->sze);
        void* ptr = __ReAlloc(nde->ptr, num, sz); // WARNING
        if (/*!_PTR_null(&ptr)*/ ptr) {
            memset(ptr, 0, sz);
            nde->sze.num = num;
            nde->sze.sze = sz ;
            nde->ptr = ptr;
            nde->typ = 0  ;
            return true ;
        }   return false;
    }

    Erno resizer_error(Morph obj, size_t ind, size_t)
    {
        if (!flag(obj, false)      ) {error NULL_ARGUMENT         ;}
        if (ind >= length_free(obj)) {error ARGUMENT_OUT_OF_BOUNDS;}
        noerror;
    }

    void* getter(Morph obj, size_t ind)
    {
        if (is_error(getter_error(obj, ind))) {return NULL;}
        return getter_free(obj, ind);
    }

    void* getter(Morph obj, size_t ind) // Retrieve a pointer to the stored data. (index)
    {
        if (_PTR_null(_PTR_arithmetic((void**)(obj->ary), ind))) {return NULL;}
        return ((Node*) (_PTR_arithmetic_rendered((void**)(obj->ary), ind)))->ptr;
    }

    Erno getter_error(Morph obj, size_t ind)
    {
        if (!flag(obj, false)      ) {error NULL_ARGUMENT         ;}
        if (ind >= length_free(obj)) {error ARGUMENT_OUT_OF_BOUNDS;}
        noerror;
    }

    void setter(Morph obj, size_t ind, void* src, size_t typ, size_t num)
    {
        if (is_error(setter_error(obj, ind, src, type, num))) {return;}
        setter_free(obj, ind, src, typ, num);
    }

    void setter_free(Morph obj, size_t ind, void* src, size_t typ, size_t num) // Put data into the container.  (index, source, type, size)
    {
        const size_t szr = sizes_free(obj, ind, false);
        Node*  ptr = (Node*)(_PTR_arithmetic_rendered((void**)(obj->ary), ind));
        void* nptr = __ReAlloc(ptr->ptr, num, szr);
        if (/*!_PTR_null(&nptr)*/ nptr) {
            memset(nptr, 0, szr*num);
            if (/*!_PTR_null(&src)*/ src) {memcpy(nptr, src, szr*num);}
            ptr->sze.num = num;
            ptr->ptr = nptr;
            ptr->typ = typ ;
            obj->err = false; return;
        }   obj->err = true ;
    }

    Erno setter_error(Morph obj, size_t ind, void* src, size_t typ, size_t num)
    {
        if (!flag(obj, false)      ) {error NULL_ARGUMENT         ;}
        if (ind >= length_free(obj)) {error ARGUMENT_OUT_OF_BOUNDS;}
        if (typ == 0 || num == 0   ) {error ZERO_ARGUMENT         ;}
        noerror;
    }

    /// Sector B

    Chest forge_new(size_t sze)
    {
        if (is_error(forge_new_error(sze))) {return NULL;}
        return forge_new_free(sze);
    }

    Chest forge_new_free(size_t sze) // Create a vessel.
    {
        Chest blk = emptier();
        if (forge(blk, sze)) {return blk;}
        dispose(blk, false);
        return NULL;
    }

    Erno forge_new_error(size_t sze)
    {
        if (!sze) {error ZERO_ARGUMENT;}
        noerror;
    }

    bool forge(Chest obj, size_t sze)
    {
        if (is_error(forge_error(obj, sze))) {return false;}
        return forge_free(obj, sze);
    }

    bool forge_free(Chest obj, size_t sze) // ReCreate a vessel.
    {
        const Coord crd = createSpair_free(sze);
        const bool  tmp = __NewNode(&(obj->nde), crd);
        free(crd);
        return tmp;
    }

    Erno forge_error(Chest obj, size_t sze)
    {
        if (!obj) {error NULL_ARGUMENT;}
        if (!sze) {error ZERO_ARGUMENT;}
        noerror;
    }

    void dispose(Chest obj, bool bl)
    {
        if (is_error(dispose_error(obj, bl))) {return;}
        dispose_error(obj, bl);
    }

    void dispose_free(Chest obj, bool bl) // UnCreate a vessel.
    {
        free(obj->nde.ptr);
        if (bl) {_Paver_free(obj);}
           else {       free(obj);}
    }

    Erno dispose_error(Chest obj, bool)
    {
        if (!obj) {error NULL_OBJECT;}
        noerror;
    }

    bool replacer(Chest from, Chest to)
    {
        if (is_error(replacer_error(from, to))) {return false;}
        return replacer_free(from, to);
    }

    bool replacer_free(Chest from, Chest to) // Copy a onto b.
    {
        return __ImposeNode(&(from->nde), &(to->nde));
    }

    Erno replacer_error(Chest from, Chest to)
    {
        if (!from) {error NULL_OBJECT  ;}
        if (!to  ) {error NULL_ARGUMENT;}
        noerror;
    }

    Chest photocopy(Chest obj)
    {
        if (is_error(photocopy_error(obj))) {return NULL;}
        return photocopy_free(obj);
    }

    Chest photocopy_free(Chest obj) // Copy a.
    {
        const Chest cht = emptier();
        if (replacer(obj, cht)) {return cht;}
        dispose(cht, false);
        return NULL;
    }

    Erno photocopy_error(Chest obj)
    {
        if (!ready(obj)) {error NULL_OBJECT;}
        noerror;
    }

    bool ready(Chest obj)
    {
        if (is_error(ready_error(obj))) {return false;}
        return ready_free(obj);
    }

    bool ready_free(Chest obj) // Retrieve a vessel's good-flag.
    {
        return !blankSpair((Coord)&(obj->nde.sze));
    }

    Erno ready_error(Chest obj)
    {
        if (/*_PTR_null((void**)&obj)*/ !obj) {error NULL_OBJECT;}
        noerror;
    }

    bool rebase(Chest obj, size_t sze)
    {
        if (is_error(rebase_error(obj, sze))) {return false;}
        return rebase_free(obj, sze);
    }

    bool rebase_free(Chest obj, size_t sze) // Change a vessel's base-size.
    {
        const size_t num = ARDD(obj->nde.sze);
        void* ral = __ReAlloc(obj->nde.ptr, num, sze);
        if (/*!_PTR_null((void**)&ral)*/ ral) {
            memset(ral, 0, num *sze);
            obj->nde.ptr     =  ral;
            obj->nde.sze.num =  num;
            return true ;
        }   return false;
    }

    Erno rebase_error(Chest obj, size_t sze)
    {
        if (/*_PTR_null((void**)&obj)*/ !obj) {error NULL_OBJECT  ;}
        if (                            !sze) {error ZERO_ARGUMENT;}
        noerror;
    }

    size_t detect(Chest obj)
    {
        if (is_error(detect_error(obj))) {return 0;}
        return detect_free(obj);
    }

    size_t detect_free(Chest obj) // Detect the data-type of a vessel's content.
    {
        return obj->nde.typ;
    }

    Erno detect_error(Chest obj)
    {
        if (/*_PTR_null((void**)&obj)*/ !obj) {error NULL_OBJECT;}
        noerror;
    }

    size_t dimensions(Chest obj, bool read)
    {
        if (is_error(dimensions_error(obj, read))) {return 0;}
        return dimensions_free(obj, read);
    }

    size_t dimensions_free(Chest obj, bool read) // Retrieve the size of a vessel.
    {
        return readSpair((Coord)&(obj->nde.sze), read);
    }

    Erno dimensions_error(Chest obj, bool)
    {
        if (/*_PTR_null((void**)&obj)*/ !obj) {error NULL_OBJECT;}
        noerror;
    }

    void* receive(Chest obj)
    {
        if (is_error(receive_error(obj))) {return NULL;}
        return receive_error(obj);
    }

    void* receive_free(Chest obj) // Retrieve the data from within a vessel.
    {
        return obj->nde.ptr;
    }

    Erno receive_error(Chest obj)
    {
        if (/*_PTR_null((void**)&obj)*/ !obj) {error NULL_OBJECT;}
        noerror;
    }

    bool send(Chest obj, void* src, size_t typ, size_t num)
    {
        if (is_error(send_error(obj, src, typ, num))) {return false;}
        return send_free(obj, src, typ, num);
    }

    bool send_free(Chest obj, void* src, size_t typ, size_t num) // Put data into a vessel. (source, type, size)
    {
        const size_t sze = ARSD(obj->nde.sze);
        void* ral = __ReAlloc(obj->nde.ptr, num, sze);
        if (/*!_PTR_null((void**)&ral)*/ ral) {
            if (/*_PTR_null((void**)&src)*/ src)
                {memset(ral, 0  , num*sze);}
            else{memcpy(ral, src, num*sze);}
            obj->nde.typ     = typ;
            obj->nde.sze.num = num;
            return true ;
        }   return false;
    }

    Erno send_error(Chest obj, void*, size_t typ, size_t num)
    {
        if (!ready(obj  )) {error NULL_OBJECT  ;}
        if (!(typ && num)) {error ZERO_ARGUMENT;}
        noerror;
    }

    /// Sector C

    Chest deposit_morph(Morph obj, size_t ind)
    {
        if (is_error(deposit_morph_error(obj, ind))) {return NULL;}
        return deposit_morph_free(obj, ind);
    }

    Chest deposit_morph_free(Morph obj, size_t ind) // Convert a Morph object into a Chest object.
    {
        Chest vsl = forge_new(sizes_free(obj, ind, false));
        if (ready(vsl)) {
            if (send(
                     vsl,
                     getter_free(obj, ind),
                     typer_free(obj, ind),
                     sizes_free(obj, ind, true))
                ) {
                return vsl;
            }
        }
        dispose(vsl, false);
        return NULL;
    }

    Erno deposit_morph_error(Morph obj, size_t sze)
    {
        if (!flag(obj, false)      ) {error NULL_OBJECT      ;}
        if (length_free(obj) <= ind) {error ARGUMENT_TOO_HIGH;}
        noerror;
    }

    Morph deposit_chest(Chest obj)
    {
        if (is_error(deposit_chest_error(obj))) {return NULL;}
        return deposit_chest_free(obj);
    }

    Morph deposit_chest_free(Chest obj) // Convert a Chest object into a Morph object.
    {
        const Coord crd = createSpair(dimensions_free(obj, false), true);
        Morph cpy = create(crd);
        free(crd);
        if (flag(cpy, false)) {
            setter(
                   cpy,
                   0,
                   receive_free(obj),
                   detect_free(obj),
                   dimensions_free(obj, true)
            );
            if (!flag(cpy, true)) {
                return cpy;
            }
        }
        destroy(cpy, false);
        return NULL;
    }

    Erno deposit_chest_error(obj)
    {
        if (!ready(obj)) {error NULL_OBJECT;}
        noerror;
    }

    /// Sector D

    Morph blank()
    {
        return blank_free();
    }

    Morph blank_free() // Make a blank container for future building upon.
    {
        Morph cpy = (Morph) malloc(sizeof(AllType));
        _Pave(cpy);
        return cpy;
    }

    Erno blank_error()
    {
        noerror;
    }

    Chest emptier()
    {
        return emptier_free();
    }

    Chest emptier_free() // Make an empty vessel for future use.
    {
        Chest vsl = (Chest) malloc(sizeof(Svar));
        _Paver(vsl);
        return vsl;
    }

    Erno emptier_error()
    {
        noerror;
    }

    /// Sector E

    Coord createSpair(size_t number, size_t size_each)
    {
        if (is_error(createSpair_error(number, size_each))) {return NULL;}
        return createSpair_free(number, size_each);
    }

    Coord createSpair_free(size_t number, size_t size_each) // Make a SPair object with a length and a depth.
    {
        Coord tmp;
        while (true) {
            tmp = (Coord) malloc(sizeof(Spair));
            if (/*!_PTR_null((void**)&tmp)*/ tmp) {break;}
        }
        tmp->num = number;
        tmp->sze = size_each;
        return tmp;
    }

    Erno createSpair_error(size_t number, size_t size_each)
    {
        if (!(number && size_each)) {error ZERO_ARGUMENT;}
        noerror;
    }

    Coord createSpair(size_t size_each, bool sz_num)
    {
        if (is_error(createSpair_error(size_each, sz_num))) {return NULL;}
        return createSpair_free(size_each, sz_num);
    }

    Coord createSpair_free(size_t size_each, bool sz_num) // Make a SPair object with a single-value length and a given depth.
    {
        Coord tmp;
        while (true) {
            tmp = (Coord) malloc(sizeof(Spair));
            if (/*!_PTR_null((void**)&tmp)*/ tmp) {break;}
        }
        tmp->num = sz_num? 1:size_each ;
        tmp->sze = sz_num?   size_each:1;
        return tmp;
    }

    Erno createSpair_error(size_t size_each, bool)
    {
        if (!size_each) {error ZERO_ARGUMENT;}
        noerror;
    }

    size_t readSpair(Coord obj, bool sizeor)
    {
        if (is_error(readSpair_error(obj, sizeor))) {return 0;}
        return readSpair_free(obj, sizeor);
    }

    size_t readSpair_free(Coord obj, bool sizeor) // Report on either a SPair object's length or it's depth.
    {
        if (sizeor) {return obj->num;}
                     return obj->sze;
    }

    Erno readSpair_error(Coord obj, bool sizeor)
    {
        if (!obj) {error NULL_OBJECT;}
        noerror;
    }

    void editSpair(Coord obj, size_t val, bool sizeor)
    {
        if (is_error(editSpair_error(obj, val, sizeor))) {return;}
        editSpair_free(obj, val, sizeor);
    }

    void editSpair_free(Coord obj, size_t val, bool sizeor) // Change either a SPair object's length or it's depth.
    {
        if (sizeor)
            {obj->num = val;}
        else{obj->sze = val;}
    }

    Erno editSpair_error(Coord obj, size_t, bool sizeor)
    {
        if (!obj   ) {error NULL_OBJECT  ;}
        if (!sizeor) {error ZERO_ARGUMENT;}
        noerror;
    }

    bool blankSpair(Coord obj)
    {
        if (is_error(blankSpair_error(obj))) {return true;}
        return blankSpair_free(obj);
    }

    bool blankSpair_free(Coord obj) // Check whether a SPair object is blank (has any zeros).
    {
        return (obj->sze == 0 ||
                obj->num == 0);
    }

    Erno blankSpair_error(Coord obj)
    {
        if (!obj) {error NULL_OBJECT;}
        noerror;
    }

}

namespace vms { /// Virtual Memory management System

    namespace {

        inline void _HST_paver_free(DPtr obj) {
            obj->ptr = NULL;
            obj->sze = 0  ;
        }

        inline Erno _HST_paver_error(DPtr obj) {
            if (/*_PTR_null((void**)&obj)*/ !obj) {error NULL_OBJECT;}
            noerror;
        }

        inline void _HST_paver_free(Cap  obj) {
            _HST_paver((DPtr)&(obj->hst));
        }

        inline Erno _HST_paver_error(Cap  obj) {
            if (/*_PTR_null((void**)&obj)*/ !obj) {error NULL_OBJECT;}
            noerror;
        }

        inline void _VMS_pave_free(HMan obj) {
            obj->loc = NULL;
            obj->hst = 0  ;
            obj->ttl = 0  ;
            obj->usd = 0  ;
        }

        inline Erno _VMS_pave_error(HMan obj) {
            if (/*_PTR_null((void**)&obj)*/ !obj) {error NO_ERROR;}
            noerror;
        }

        inline DPtr _HMN_ptr_extract(HMan obj, size_t ind) {
            size_t*     loc = (size_t*) (_PTR_counting((obj->loc), ind));
            const DPtr ploc = (DPtr   ) (_PTR_counting((obj->loc), sizeof(Host) * (*(loc++))));
            if (ploc->ptr == loc) {return ploc;} return NULL;
        }

        inline Erno _HMN_ptr_extract(HMan obj, size_t ind) {
            if (/*_PTR_null((void**)&obj)*/ !obj ) {error NULL_OBJECT      ;}
            if ((obj->usd) > ind + sizeof(size_t)) {error ARGUMENT_TOO_HIGH;}
            noerror;
        }

        inline bool _HMN_ptr_member(HMan obj, DPtr ptr) {
            const size_t ploc = (*(size_t*)(obj->loc));
            const size_t qloc =    ploc +   obj->ttl  ;
            const size_t xloc = (*(size_t*)(ptr->ptr)) - sizeof(size_t);
            const size_t yloc =    xloc +   ptr->sze   + sizeof(size_t);
            //
            if (ploc <= xloc && qloc >= yloc) {return _HMN_ptr_extract(obj, xloc) == ptr;}
            return false;
        }

        inline Erno _HMN_ptr_member(HMan obj, DPtr ptr) {
            if (!obj) {error NULL_OBJECT  ;}
            if (!ptr) {error NULL_ARGUMENT;}
            noerror;
        }

    }

    /// Sector A

    HMan make_new(Cap obj) // Allocate a VMS.
    {
        if (is_error(make_new_error(obj))) {return NULL;}
        return make_new_free(obj);
    }

    HMan make_new_free(Cap obj) // Allocate a VMS.
    {
        const HMan nw = (HMan) malloc(sizeof(Heap));
        if ((get_sz_free(obj) >
            sizeof(Host) + sizeof(size_t)) &&
            /*!_PTR_null((void**)&nw)*/ nw) {
            nw->loc = obj->hst.ptr;
            nw->ttl = obj->hst.sze;
            nw->usd = 0;
            nw->hst = 0;
            free(obj);
            return nw;
        }
        free(nw);
        return NULL;
    }

    Erno make_new_error(Cap obj) {
        if (!stable(obj)) {error NULL_OBJECT;}
        noerror;
    }

    HMan make_new(size_t sze)
    {
        if (is_error(make_new_error(sze))) {return NULL;}
        return make_new(sze);
    }

    HMan make_new_free(size_t sze) // Allocate a VMS with a designated size.
    {
        const Cap nw = capturer_free(sze);
        HMan tmp = make_new(nw);
        if (!tmp) {abort_cap(nw, false);}
        return tmp;
    }

    Erno make_new_error(size_t sze)
    {
        if (!sze) {error ARGUMENT_TOO_LOW;}
        noerror;
    }

    bool resizing(HMan obj, size_t sze)
    {
        if (is_error(resizing_error(obj, sze))) {return false;}
        return resizing_free(obj, sze);
    }

    bool resizing_free(HMan obj, size_t sze) // Expand or shrink a VMS.
    {
        if (get_used_free(obj)) {
            if (sze <  obj->usd) {return false;}
            if (sze == obj->ttl) {return true;}
            void* tmp = realloc(obj->loc, sze);
            if (/*!_PTR_null((void**)&tmp)*/ tmp) {
                obj->loc = tmp;
                obj->ttl = sze;
                return true;
            }
        }
        return false;
    }

    Erno resizing_error(HMan obj, size_t sze)
    {
        if (is_error(get_used_error(obj))) {error NULL_OBJECT;}
        if (!sze) {error ZERO_ARGUMENT;}
        noerror;
    }

    size_t compress(HMan obj, size_t cnt, size_t srt, size_t end, bool forward)
    {
        if (is_error(compress_error(obj, cnt, srt, end, forward))) {return 0;}
        return compress_free(obj, cnt, srt, end, forward);
    }

    size_t compress_free(HMan obj, size_t cnt, size_t srt, size_t end, bool forward) // Eliminate unallocated bytes between allocated blocks. (min_byte_clear, startByte, endByte, direction)
    {
        const size_t used = get_used_free(obj);
            size_t total, srr, freed=0, i,j,k ;
            void* srr0; void* srr1; void* srr2;
            DPtr tmp;  DPtr tmp1 ;
            Host fst = {NULL, 0} ;
            bool first=true, done;
        // 15 Variables! Yikes!
        if (forward) {
            if (!end) {end = used;}
            ///
            const size_t hst = obj->hst * sizeof(Host);
            const size_t edr = hst + end;
            k = hst;
            for (i = hst + srt; i < edr; i++) {
                if (first) {tmp = &fst;}
                else {
                    tmp = _HMN_ptr_extract(obj, i);
                    if (/*_PTR_null((void**)&tmp)*/ !tmp) {continue;}
                }
                srr = i;
                if (!first) {srr += sizeof(size_t) + (tmp->sze);}
                done = true;
                for (j = srr; j < edr; j++) {
                    tmp1 = _HMN_ptr_extract(obj, j);
                    if (/*!_PTR_null((void**)&tmp1)*/ tmp1) {
                        total = sizeof(size_t) + tmp1->sze;
                        srr0 = _PTR_counting(obj->loc, srr);
                        srr2 = _PTR_counting(obj->loc, j  );
                        for (k = total; k > 0;) {
                            srr1 = _PTR_counting(srr2, k-1);
                            memcpy(_PTR_counting(srr0, --k), srr1, sizeof(char));
                            memset(srr1, 0, sizeof(char));
                        }
                        i += sizeof(size_t) + tmp->sze - 1;
                        k = i + 1;
                        done = false;
                        freed += j - srr;
                        tmp1->ptr = _PTR_counting(obj->loc, k + sizeof(size_t)); // WARNING
                        break;
                    }
                }
                if (done) {break;}
                if (first) {first = false;}
            }
            k -= hst;
            if (end == used) {obj->usd -= freed;}
        } else {
            if (!srt) {srt = used;}
            ///
            const size_t hst = obj->hst * sizeof(Host);
            const size_t ddt = sizeof(size_t) + 1;
            const size_t edr = hst + end;
            k = hst;
            for (i = hst + srt; i >= edr; i--) { // WARNING
                if (first) {tmp = &fst;}
                else {
                    tmp = _HMN_ptr_extract(obj, i);
                    if (/*_PTR_null((void**)&tmp)*/ !tmp) {continue;}
                }
                srr = i;
                if (!first) {srr -= srr < ddt? 1:ddt;}
                done = true;
                for (j = srr; j >= edr; j--) {
                    tmp1 = _HMN_ptr_extract(obj, j);
                    if (/*!_PTR_null((void**)&tmp1)*/ tmp1) {
                        total = sizeof(size_t) + tmp1->sze;
                        srr0 = _PTR_counting(obj->loc, i - total);
                        srr2 = _PTR_counting(obj->loc, j        );
                        for (k = total; k > 0;) {
                            srr1 = _PTR_counting(srr2, k-1);
                            memcpy(_PTR_counting(srr0, --k), srr1, sizeof(char));
                            memset(srr1, 0, sizeof(char));
                        }
                        i -= total - 1;
                        k = i - 1;
                        done = false;
                        freed += i-j -total;
                        tmp1->ptr = _PTR_counting(obj->loc, k + sizeof(size_t)); // WARNING
                        break;
                    }
                }
                if (done) {break;}
                first = false;
            }
            k -= hst;
        }
        ///
        if (cnt) {
            if (freed >= cnt) {return k;}
            return 0;
        }
        return k;
    }

    Erno compress_error(HMan obj, size_t cnt, size_t srt, size_t end, bool forward)
    {
        if (!sturdy(obj)) { error NULL_OBJECT; }
        const size_t used = get_used_free(obj);
        if (!used) { error ZERO_ARGUMENT; }
        if (forward) {
            if (!end) {end = used;}
            if (srt >= end) { error BOUNDS_ARGUMENT_WRONG_ORDER; }
            if (cnt >  end - srt) { error ARGUMENT_TOO_HIGH; }
        } else {
            if (!srt) {srt = used;}
            if (end >= srt) { error BOUNDS_ARGUMENT_WRONG_ORDER; }
            if (cnt >  srt - end) { error ARGUMENT_TOO_HIGH; }
        }
        noerror;
    }

    void release(HMan obj) // Deallocate a VMS.
    { if (_PTR_null((void**)&obj)) {return;}
        free(obj->loc);
        free(obj);
    }

    size_t get_size(HMan obj) // Report the number of bytes in the VMS.
    { if (_PTR_null((void**)&obj)) {return 0;}
        return obj->ttl;
    }

    size_t get_used(HMan obj) // Report the number of used bytes in the VMS.
    { if (_PTR_null((void**)&obj)) {return 0;}
        return obj->usd;
    }

    size_t get_hsts(HMan obj) // Report the number of allocations tied to the VIB.
    { if (_PTR_null((void**)&obj)) {return 0;}
        return obj->hst;
    }

    bool sturdy(HMan obj) // Verify the successful allocation of a VMS.
    {
        if (get_size(obj)) {
            return !_PTR_null((void**)&(obj->loc));
        }   return false;
    }

    DPtr issue_alloc(HMan obj, size_t sze, bool pse, size_t srt, size_t end) // Request a piece of memory from a VMS. (size, parse_or_append, startHost, endHost)
    { if (!(sturdy(obj) && sze)) {return NULL;}
      if (!end) {end = obj->hst;}
      if (srt > end || end > obj->hst) {return NULL;}
        DPtr ptr0;
        size_t loc;
        if (pse) {
            const size_t for_srt = srt * sizeof(Host);
            const size_t for_end = end * sizeof(Host);
            bool fnd=true;
            for (size_t i=for_srt; i <= for_end; i+=sizeof(Host)) {
                ptr0 = (DPtr) _PTR_render(_PTR_arithmetic((void**)&(obj->loc), i));
                if (!valid_ptr(ptr0)) {
                    fnd = false;
                    loc = i;
                    break;
                }
            }
            if (fnd /* not found */) {return NULL;}
        } else {
            const size_t stt = ((obj->hst    ) * sizeof(Host));
            const size_t ett = ((obj->hst + 1) * sizeof(Host));
            for (size_t i=stt; i < ett; i++) {
                ptr0 = _HMN_ptr_extract(obj, i);
                if (valid_ptr(ptr0)) {return NULL;}
            }
            ptr0 = (DPtr) (_PTR_counting((obj->loc), stt));
            loc = stt;
        }
        if (obj->ttl - obj->usd >= sizeof(size_t) + sze) {
            Host tmp;
            tmp.sze = sze;
            size_t* usd = (size_t*) (_PTR_counting((obj->loc), obj->usd));
            *usd = loc / sizeof(Host);
            tmp.ptr = (void*) (usd + 1);
            obj->usd += sizeof(size_t) + sze;
            obj->hst += 1;
            ptr0->ptr = tmp.ptr;
            ptr0->sze = tmp.sze;
            return ptr0;
        }   return NULL;
    }

    DPtr reissue_alc(HMan obj, DPtr ptr, size_t sze, bool sch) // Change the size of an allocation. (search_or_move)
    { if (!sze) {
        free_alloct(obj, ptr);
        return NULL;
      }
      if (!sturdy   (obj)) {return NULL;}
      if (!valid_ptr(ptr)) {return NULL;}
      if (ptr->sze == sze) {return ptr ;}
        const size_t srt = _PTR_subtact(obj->loc, ptr->ptr) + ptr->sze;
        if (sch) {
            if (ptr->sze < sze) {
                const size_t eda = srt + (ptr->sze - sze);
                if (eda > (obj->ttl)) {return NULL;}
                for (size_t i=srt; i<eda; i++) {
                    if (valid_ptr(_HMN_ptr_extract(obj, i))) {return NULL;}
                }
                if (eda > (obj->usd)) {obj->usd = eda;}
            } else if (ptr->sze != sze) {
                memset(_PTR_render(_PTR_arithmetic((void**)&(ptr->ptr), sze)), 0, ptr->sze - sze);
                if (srt + 1 == obj->usd) {obj->usd -= ptr->sze - sze;}
            }
        } else {
            if (ptr->sze < sze) {
                if (obj->ttl -  obj->usd >= sizeof(size_t) + sze) {
                    void* usd = _PTR_render(_PTR_arithmetic((void**)&(obj->loc),obj->usd));
                    void* dst = _PTR_render(_PTR_arithmetic((void**)&(obj->loc), _PTR_subtact(obj->loc, ptr->ptr) - sizeof(size_t)));
                    const size_t sso = sizeof(size_t) + sze;
                    memcpy(usd , dst   , sso);
                    memset(      dst, 0, sso);
                    ptr->ptr = (void*) ((size_t*)(usd) + 1);
                    obj->usd += sso;
                } else { return NULL; }
            } else if (ptr->sze != sze) {
                memset(_PTR_render(_PTR_arithmetic((void**)&(ptr->ptr), sze)), 0, ptr->sze - sze);
                if (srt + 1 == obj->usd) {obj->usd -= ptr->sze - sze;}
            }
        }
        ptr->sze = sze;
        return ptr;
    }

    void free_alloct(HMan obj, DPtr ptr) // Free an allocation from a VMS.
    { if (!sturdy   (obj)) {return;}
      if (!valid_ptr(ptr)) {return;}
        ///
        const void*   dst = _PTR_counting((void**)&(obj->loc), _PTR_subtact(obj->loc, ptr->ptr) - sizeof(size_t));
        const size_t  dif = _PTR_subtact(obj->loc, (void*)dst);
        const size_t  inr = sizeof(size_t) + ptr->sze;
        const size_t  ind = *((size_t*)dst);
        ///
        memset((void*)dst, 0, inr);
        ptr->ptr = NULL;
        ptr->sze =  0  ;
        ///
        if (dif + inr == obj->usd) {obj->usd  = dif;}
        if (ind +  1  == obj->hst) {obj->hst -=  1 ;}
    }

    bool change_index(HMan obj, DPtr ptr, size_t ind) // Change the index of a DynamicPointer in a VMS.
    { if (!sturdy   (obj)) {return false;}
      if (!valid_ptr(ptr)) {return false;}
      if (ind >= obj->hst) {return false;}
        ///
        const DPtr onto = (DPtr) _PTR_render(_PTR_arithmetic((void**)(obj->loc), sizeof(Host) * ind));
        if (valid_ptr(onto)) {return false;}
        ///
        *((size_t*) _PTR_render(_PTR_arithmetic((void**)(obj->loc), _PTR_subtact(obj->loc, ptr->ptr) - sizeof(size_t)))) = ind;
        onto->ptr = ptr->ptr;
        onto->sze = ptr->sze;
        ptr ->ptr =   NULL ;
        ptr ->sze =    0   ;
        return true;
    }

    size_t get_index(HMan obj, DPtr ptr, bool inx) // Determine the index of a DynamicPointer in a VMS. (pointer, index_or_not)
    { if (!sturdy   (obj)) {return 0;}
      if (!valid_ptr(ptr)) {return 0;}
        if (inx) {
            return *(size_t*) _PTR_render(_PTR_arithmetic((void**)&(obj->loc), _PTR_subtact(obj->loc, ptr->ptr) - sizeof(size_t)));
        } else {
            return _PTR_subtact(obj->loc, ptr->ptr);
        }
    }

    void* get_dptr(DPtr ptr) // Get the static pointer from a DynamicPointer.
    { if (_PTR_null((void**)&ptr)) {return NULL;}
        return ptr->ptr;
    }

    bool valid_ptr(DPtr ptr) // Determine if a DynamicPointer is valid.
    { if (_PTR_null((void**)& ptr      )) {return false;}
      if (_PTR_null((void**)&(ptr->ptr))) {return false;}
        return true;
    }

    DPtr find_dptr(HMan obj, size_t ind) // Find a DynamicPointer on the Rails by index.
    { if (ind >= get_size(obj)) {return NULL;}
        return (DPtr) _PTR_render(_PTR_arithmetic((void**)&(obj->loc), sizeof(Host) * ind));
    }

    size_t index_get(HMan obj, DPtr ptr) // Get the index of a DynamicPointer on the Rails.
    { return get_index(obj, ptr, true); }

    /// Sector B

    Cap capturer(size_t sze) // Allocate a Capturer. (size)
    { if (!sze) {return NULL;}
        Cap tmp = (Cap) malloc(sizeof(Capture)); _HST_paver(tmp);
        if (!_PTR_null((void**)&tmp)) {
            tmp->hst.ptr = calloc(sze, sizeof(char));
            if (!_PTR_null((void**)&(tmp->hst.ptr))) {
                tmp->hst.sze = sze;
                return tmp;
            }
        }
        free(tmp);
        return NULL;
    }

    bool grow_cap(Cap obj, size_t sze) // Capture more memory (specified).
    { if ((!stable(obj)) || sze == 0) {return false;}
        const size_t base    = obj->hst.sze;
        const size_t length1 = base   + sze;
        void* tmp = realloc(obj->hst.ptr, length1);
        if (!_PTR_null((void**)&tmp)) {
            obj->hst.ptr = tmp    ;
            obj->hst.sze = length1;
            memset(tmp, 0, length1);
            return  true;
        }   return false;
    }

    bool shrinker(Cap obj, size_t sze) // Release memory (specified).
    { if (sze >= get_sz(obj)) {return false;}
      if (!sze) {return true;}
        const size_t length1= obj->hst.sze - sze;
        void* tmp = realloc(obj->hst.ptr, length1);
        if (!_PTR_null((void**)&tmp)) {
            obj->hst.ptr = tmp   ;
            obj->hst.sze = length1;
            return  true;
        }   return false;
    }

    void abort_cap(Cap obj, bool pve) // Free the contents of a Capturer. (pave_or_free)
    { if (_PTR_null((void**)&obj)) {return;}
        free(obj->hst.ptr);
        if (pve) {_HST_paver(obj);}
        else     {      free(obj);}
    }

    bool stable(Cap obj) // Check if a Capturer was allocated successfully.
    { if (_PTR_null((void**)&obj)) {return false;}
        return !_PTR_null((void**)&(obj->hst.ptr));
    }

    size_t get_sz(Cap obj) // Get the current size of a Capturer.
    { if (_PTR_null((void**)&obj)) {return 0;}
        return obj->hst.sze;
    }

}

