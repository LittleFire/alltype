#include "main.hpp"

/*
    type scheme
    1 char
    2 char*
    3 int
    4 int*
    5 float
    6 float*
*/

int main(void) {
    printf("Test (non formal)\n\n"); fflush(stdout);
    srand(time(NULL));

    HMan obj = vms::make_new((size_t)100);
    if (vms::sturdy(obj)) {
        //
        DPtr igr = vms::issue_alloc(obj, sizeof(int), true, 0, 0);
        if (vms::valid_ptr(igr)) {
            int* itr = (int*) vms::get_dptr(igr);
            *itr = 54321;
            //
            printf("Integer reads: %d\n", *((int*) vms::get_dptr(igr)));
            fflush(stdout);
        }
        //
        printf("Used %u of %u.\n", vms::get_used(obj), vms::get_size(obj));
        fflush(stdout);
        //
        vms::free_alloct(obj, igr);
        //
        printf("Used %u of %u.\n", vms::get_used(obj), vms::get_size(obj));
        fflush(stdout);
        ///
        DPtr str = vms::issue_alloc(obj, sizeof(char)*11, true, 0, 0);
        //
        if (vms::valid_ptr(str)) {
            char* stg = (char*) vms::get_dptr(str);
            strcpy(stg, "Hello you!");
            //
            printf("String reads: %s\n", (char*) vms::get_dptr(str));
            fflush(stdout);
        }
        //
        printf("Used %u of %u.\n", vms::get_used(obj), vms::get_size(obj));
        fflush(stdout);
    }
    vms::release(obj);

    printf("\n Done.\n"); fflush(stdout);
    #ifndef DEBUG
        getch(); fflush(stdin);
    #endif // DEBUG
    return 0;
}

