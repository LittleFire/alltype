#include <cstdlib>
#include <cstring>
#include <stdio.h>
#include <conio.h>

main() {
	// pointers & types
	void* a=NULL; void* b=NULL;
	bool ta     ,      tb     ;
	
	int intbool; while (true) {
		printf("Data type #1 [1 char / 2 float]: "); fflush(stdout); intbool = 0;
		scanf("%d", &intbool); fflush(stdin);
		if (intbool == 1 || intbool == 2) {
			ta = (intbool==1);
			break;
		}
	} while (true) {
		printf("Data type #2 [1 char / 2 float]: "); fflush(stdout); intbool = 0;
		scanf("%d", &intbool); fflush(stdin);
		if (intbool == 1 || intbool == 2) {
			tb = (intbool==1);
			break;
		}
	}

	if (ta /* char */) {
		printf("Char for #1: "); fflush(stdout);
		char Ca; while (true) {
			Ca = getch();
			if (('a' <= Ca and 'z' >= Ca) or
				('A' <= Ca and 'Z' >= Ca)) {
				a = malloc(sizeof(char));
				if (a) {
					memcpy(a, &Ca, sizeof(char));
					printf("%c\n", Ca); fflush(stdout);
					break;
				} else {
					fprintf(stderr, "<MemError>\n"); fflush(stderr);
					exit(1);
				}
			}
		}
	} else {
		float f; while (true) {
			printf("Floating-point value #1: "); fflush(stdout); f = 0;
			scanf("%f", &f); fflush(stdout);
			if (f > 0) {
				a = malloc(sizeof(float));
				if (a) {
					memcpy(a, &f, sizeof(float));
					break;
				} else {
					fprintf(stderr, "<MemError>\n"); fflush(stderr);
					exit(2);
				}
			}
		}
	}

	if (tb /* char */) {
		printf("Char for #2: "); fflush(stdout);
		char Cb; while (true) {
			Cb = getch();
			if (('a' <= Cb and 'z' >= Cb) or
				('A' <= Cb and 'Z' >= Cb)) {
				b = malloc(sizeof(char));
				if (b) {
					memcpy(b, &Cb, sizeof(char));
					printf("%c\n", Cb); fflush(stdout);
					break;
				} else {
					fprintf(stderr, "<MemError>\n"); fflush(stderr);
					free(a);
					exit(3);
				}
			}
		}
	} else {
		float f; while (true) {
			printf("Floating-point value #2: "); fflush(stdout); f = 0;
			scanf("%f", &f); fflush(stdout);
			if (f > 0) {
				b = malloc(sizeof(float));
				if (b) {
					memcpy(b, &f, sizeof(float));
					break;
				} else {
					fprintf(stderr, "<MemError>\n"); fflush(stderr);
					free(a);
					exit(4);
				}
			}
		}
	}

	printf("\n"); fflush(stdout);

	if (ta /* char */) { printf("Char #1 is: %c\n"             , *((char *)a)); fflush(stdout); }
	else               { printf("Floating-point value #1: %f\n", *((float*)a)); fflush(stdout); }
	if (tb /* char */) { printf("Char #2 is: %c\n"             , *((char *)b)); fflush(stdout); }
	else               { printf("Floating-point value #2: %f\n", *((float*)b)); fflush(stdout); }

	free(a); free(b);
	getch();
}
