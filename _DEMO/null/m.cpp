#include <cstdlib>
#include <cstdio>

int main(void) {
    char* str = (char*) calloc(5, sizeof(char));
    if (str) {
        str[0] = 'F' ;
        str[1] = 'o' ;
        str[2] = 'u' ;
        str[3] = 'r' ;
        str[4] = NULL;
        
        printf("%s\n", str);
        fflush(stdout);
        
        char* str0 = (char*) realloc(str, 4*sizeof(char));
        if (str0) {
            str[0] = 'F' ;
            str[1] = 'u' ;
            str[2] = 'n' ;
            str[3] = NULL;
            
            printf("%s\n", str0);
            fflush(stdout);
            
            free(str0);
        } else {
            free(str );
        }
    }
    return 0;
}
