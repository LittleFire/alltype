
    HMan obj = vms::make_new((size_t)100);
    if (vms::sturdy(obj)) {
        DPtr str = vms::issue_alloc(obj, sizeof(char)*11, false, 0, 0);
        DPtr igr = vms::issue_alloc(obj, sizeof(int )   , false, 0, 0);
        //
        if (vms::valid_ptr(str)) {
            char* stg = (char*) vms::get_dptr(str);
            strcpy(stg, "Hello you!");
        }
        if (vms::valid_ptr(igr)) {
            int* itr = (int*) vms::get_dptr(igr);
            *itr = 54321;
        }
        //
        if (vms::valid_ptr(igr)) {
            printf("Integer reads: %d\n", *((int*) vms::get_dptr(igr)));
            fflush(stdout);
        }
        if (vms::valid_ptr(str)) {
            printf("String reads: %s\n", (char*) vms::get_dptr(str));
            fflush(stdout);
        }
    }
    //
    printf("Used %zu of %zu.\n", vms::get_used(obj), vms::get_size(obj));
    fflush(stdout);
    //
    vms::release(obj);
