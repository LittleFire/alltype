
    Morph a = alt::blank();
    size_t ln; Coord newln; while (true) {
        printf("Length: ");
        fflush(stdout);
        ln = 0;
        scanf("%i", &ln);
        fflush(stdin);
        if (ln > 0) { newln = alt::createSpair(ln, false);
               if (alt::recreate(a, newln)) {
                   free(newln);
                   break;
               }   free(newln);
                   alt::destroy(a, true);
               fprintf(stderr, "Memory deficiency.\n"); fflush(stderr);
        } else fprintf(stderr, "Non-zero input.\n"   ); fflush(stderr);
    } unsigned short int type; for (size_t i=0; i<ln; i++) {
        while (true) {
            printf("Type for element #%i: ", i);
            fflush(stdout);
            type = 0;
            scanf("%hu", &type);
            fflush(stdin);
            if (type > 0 and type < 4) {break;}
        } switch (type) {
            case 1: {
                char input;
                    printf("Value for element %i [char]: ", i);
                    fflush(stdout);
                    while (true) {
                        input = getch();
                        fflush(stdin);
                        if ((input >= 'A' and input <= 'Z') or
                            (input >= 'a' and input <= 'z')) {
                            printf("%c\n", input);
                            fflush(stdout);
                            break;
                        }
                    }
                if (alt::resizer(a, i, (size_t)sizeof(char))) {
                    alt::setter(a, i, &input, 1, 1);
                    if (!alt::flag(a, true)) {
                        printf("\tThe letter %c was put in the array at index %i successfully.\n", input, i);
                        fflush(stdout);
                    } else {
                        fprintf(stderr, "\tMemory deficiency.\n");
                        fflush(stderr);
                    }
                } else {
                    fprintf(stderr, "\tMemory deficiency.\n");
                    fflush(stderr);
                }
                break;
            }
            case 2: {
                char str[BUFSIZ]; for (size_t x=0; x<BUFSIZ; x++) {str[x] = '\0';}
                    while (true) {
                        printf("Non-empty string for element %i [char*]: ", i); fflush(stdout);
                        fgets(str, BUFSIZ, stdin); fflush(stdin);
                        bool err=false;
                            for (size_t j=0; j<strlen(str); j++) {
                                if (!((str[j]>='a' and str[j]<='z') or
                                      (str[j]>='A' and str[j]<='Z') or
                                      (str[j]=='\n' or str[j]==' '))) {
                                    err = true;
                                    break;
                                }
                            } if (err) {continue;}
                            str[strlen(str)-1] = '\0';
                            break;
                    }
                if (alt::resizer(a, i, (size_t)sizeof(char))) {
                    alt::setter(a, i, str, 2, (size_t)(strlen(str)+1));
                    if (!alt::flag(a, true)) {
                        printf("\tThe string \"%s\" was put in the array at index %i successfully.\n", str, i);
                        fflush(stdout);
                    } else {
                        fprintf(stderr, "\tMemory deficiency.\n");
                        fflush(stderr);
                    }
                } else {
                    fprintf(stderr, "\tMemory deficiency.\n");
                    fflush(stderr);
                }
                break;
            }
            case 3: {
                int input;
                    while (true) {
                        printf("Integral value for element %i [int]: ", i);
                        fflush(stdout);
                        input = 0;
                        scanf("%d", &input);
                        fflush(stdin);
                        if (input > 0) {break;}
                    }
                if (alt::resizer(a, i, (size_t)sizeof(int))) {
                    alt::setter(a, i, &input, 3, 1);
                    if (!alt::flag(a, true)) {
                        printf("\tThe integer %d was put in the array at index %i successfully.\n", input, i);
                        fflush(stdout);
                    } else {
                        fprintf(stderr, "\tMemory deficiency.\n");
                        fflush(stderr);
                    }
                } else {
                    fprintf(stderr, "\tMemory deficiency.\n");
                    fflush(stderr);
                }
            }
        }
    }
    putc('\n', stdout);
        for (size_t i=0; i<alt::length(a); i++) {
            printf("Element #%i is a", i);
            switch (alt::typer(a, i)) {
                case 0: {
                    printf(" NULL");
                    break;
                }
                case 1: {
                    printf(" character that reads '%c'", *((char*)alt::getter(a, i)));
                    break;
                }
                case 2: {
                    printf(" string that reads \"%s\"" ,   (char*)alt::getter(a, i) );
                    break;
                }
                case 3: {
                    printf("n integer set to %d"       , *((int *)alt::getter(a, i)));
                }
            }
            printf(".\n");
            fflush(stdout);
        }
    alt::destroy(a, false);
