
    Morph a = alt::create((size_t)sizeof(char));
    if (alt::flag(a, false)) { char c='A';
        alt::setter(a, 0, &c, 1, 1);
        char* b = (char*) alt::getter(a, 0);
        printf("%c %u\n", b!=NULL? (*b):'?', alt::typer(a, 0));
        fflush(stdout);
        if (alt::resizer(a, 0, (size_t)sizeof(int))) {
            int C=1234;
            alt::setter(a, 0, &C, 3, 1);
            int* B = (int*) alt::getter(a, 0);
            printf("%d %u\n", B!=NULL? (*B):0, alt::typer(a, 0));
            fflush(stdout);
        } else {
            fprintf(stderr, "Resize error.\n");
            fflush(stdout);
        }
    } else {
        fprintf(stderr, "Error.\n");
        fflush(stderr);
    }
    alt::destroy(a, false);
