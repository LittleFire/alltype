
    HMan obj = vms::make_new((size_t)100);
    if (vms::sturdy(obj)) {
        DPtr str = vms::issue_alloc(obj, sizeof(char)*6, false, 0, 0);
        DPtr igr = vms::issue_alloc(obj, sizeof(int )  , false, 0, 0);
        //
        if (vms::valid_ptr(str)) {
            char* stg = (char*) vms::get_dptr(str);
            strcpy(stg, "Hello");
            printf("String reads: %s\n", stg);
            fflush(stdout);
        }
        if (vms::valid_ptr(igr)) {
            int* itr = (int*) vms::get_dptr(igr);
            *itr = 54321;
            printf("Integer reads: %d\n", *itr);
            fflush(stdout);
        }
    }
    vms::release(obj);
