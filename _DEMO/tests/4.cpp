
    Coord test = alt::createSpair((size_t)1, (size_t)2);
        printf("Created {%i x %i};\n", alt::readSpair(test, false), alt::readSpair(test, true)); fflush(stdout);
        alt::editSpair(test, 3, false); alt::editSpair(test, 4, true);
        printf("Reset   {%i x %i};\n", alt::readSpair(test, false), alt::readSpair(test, true)); fflush(stdout);
    free(test);

    test = alt::createSpair(5, true);
        printf("Created {%i x %i};\n", alt::readSpair(test, false), alt::readSpair(test, true)); fflush(stdout);
    free(test);

    test = alt::createSpair(5, false);
        printf("Reset   {%i x %i};\n", alt::readSpair(test, false), alt::readSpair(test, true)); fflush(stdout);
    free(test);

    putchar('\n'); fflush(stdout);

    test = alt::createSpair((size_t)0, (size_t)0);
        printf("Is%s blank\n", alt::blankSpair(test)? "":" not"); fflush(stdout);
        alt::editSpair(test, 1, true);
        printf("Is%s blank\n", alt::blankSpair(test)? "":" not"); fflush(stdout);
        alt::editSpair(test, 0, true );
        alt::editSpair(test, 1, false);
        printf("Is%s blank\n", alt::blankSpair(test)? "":" not"); fflush(stdout);
        alt::editSpair(test, 1, true);
        printf("Reset   {%i x %i};\n", alt::readSpair(test, false), alt::readSpair(test, true)); fflush(stdout);
    free(test);
