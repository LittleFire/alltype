#include "main.hpp"

/*
    type scheme
    1 char
    2 char*
    3 int
    4 int*
    5 float
    6 float*
*/

void display(const char*, Morph);
void make(Morph, size_t);

int main(void) {
    printf("Test (non formal)\n\n"); fflush(stdout);
    srand(time(NULL));

    Morph hold = alt::blank();
    {
        size_t input;
        while (true) {
            printf("Length: "); fflush(stdout);
            input = 0; scanf("%i", &input); fflush(stdin);
            if (input > 0) {break;}
        }
        Coord dimx = alt::createSpair(input, (size_t)1);
        alt::recreate(hold, dimx); free(dimx);
    }
    if (!alt::flag(hold, true)) {
        for (size_t i=0; i<alt::length(hold); i++) {make(hold, i);} display("First made", hold);
        ///
        Morph object = alt::make_copy(hold);
        if (alt::flag(object, false)) {
            display("Copied", object);
            for (size_t i=0; i<alt::length(object); i++) {
                if (rand()%2) {
                    if (alt::inserter(object, i)) {make(object, i);}
                    else {
                        fprintf(stderr, "Insertion @%i failed.\n", i);
                        fflush(stderr);
                    }
                }
            }
            ///
            display("Lengthened", object);
            if (!alt::copy_onto(object, hold)) {
                fprintf(stderr, "\tImposition failed.\n");
                fflush(stderr);
            } else display("Imposed", object);
            alt::destroy(object, false);
        }
    } else {
        fprintf(stderr, "\tMemory deficiency.\n");
        fflush(stderr);
    }
    ///
    Coord ar[3]; for (size_t i=0; i<3; i++) {ar[i] = alt::createSpair((size_t)0, (size_t)0);}
    for (size_t i=0; i<alt::length(hold); i++) {
        switch (alt::typer(hold, i)) {
            case 1: {
                if (alt::blankSpair(ar[0])) {
                    alt:: editSpair(ar[0], alt::sizes(hold, i, false), false);
                    alt:: editSpair(ar[0], alt::sizes(hold, i, true ), true );
                }
                break;
            }
            case 3: {
                if (alt::blankSpair(ar[1])) {
                    alt:: editSpair(ar[1], alt::sizes(hold, i, false), false);
                    alt:: editSpair(ar[1], alt::sizes(hold, i, true ), true );
                }
                break;
            }
            case 5: {
                if (alt::blankSpair(ar[2])) {
                    alt:: editSpair(ar[2], alt::sizes(hold, i, false), false);
                    alt:: editSpair(ar[2], alt::sizes(hold, i, true ), true );
                }
            }
        }
    }
    ///
    {
        size_t byt; putchar('\n');
        if (!alt::blankSpair(ar[0])) {
            byt = alt::readSpair(ar[0], false);
            printf("A  character is %i byte%s.\n", byt, byt>1? "s":"");
            fflush(stdout);
        }
        if (!alt::blankSpair(ar[1])) {
            byt = alt::readSpair(ar[1], false);
            printf("An integer   is %i byte%s.\n", byt, byt>1? "s":"");
            fflush(stdout);
        }
        if (!alt::blankSpair(ar[2])) {
            byt = alt::readSpair(ar[2], false);
            printf("A  float-num is %i byte%s.\n", byt, byt>1? "s":"");
            fflush(stdout);
        }
    }
    for (size_t i=0; i<3; i++) {free(ar[i]);}
    alt::destroy(hold, false);

    printf("\n Done.\n"); fflush(stdout);
    #ifndef DEBUG
        getch(); fflush(stdin);
    #endif // DEBUG
    return 0;
}

bool check_null(void* ptr, size_t sz) {
    for (size_t i=0; i<sz; i++) {
        if ((*(((char*)(ptr))+i)) != '\0') {
            return false;
        }
    }
    return true;
}

void display(const char* lbl, Morph obj) {
    const size_t len = alt::length(obj);
    printf("%s %i: { ", lbl, len); fflush(stdout);
    if (len == 0) {printf("NONE");}
    else { void* p;
        for (size_t i=0; i<len;) {
            p = alt::getter(obj, i);
            if (p != NULL) {
                if (check_null(p, alt::sizes(obj, i, false) * alt::sizes(obj, i, true))) {
                    printf("....");
                } else {
                    switch (alt::typer(obj, i)) {
                        case 0: {
                            printf("NULL");
                            break;
                        }
                        case 1: {
                            printf("%c", (*((char *)p)));
                            break;
                        }
                        case 3: {
                            printf("%d", (*((int  *)p)));
                            break;
                        }
                        case 5: {
                            printf("%f", (*((float*)p)));
                        }
                    }
                }
                if (++i != len) {printf(" , ");}
            } else {
                printf("null");
                i++;
            }
            fflush(stdout);
        }
    }
    printf(" }\n");
    fflush(stdout);
}

void make(Morph obj, size_t ind) {
    switch (rand()%3) {
        case 0: { // 1
            char src = 'A'+rand()%26;
            if (alt::resizer(obj, ind, (size_t)sizeof(char )))
            { alt::setter(obj, ind, &src, 1, 1); }
            break;
        }
        case 1: { // 3
            int igr = rand();
            if (alt::resizer(obj, ind, (size_t)sizeof(int  )))
            { alt::setter(obj, ind, &igr, 3, 1); }
            break;
        }
        case 2: { // 5
            float dec = (float) (rand()+1) / (float) (rand()+1);
            if (alt::resizer(obj, ind, (size_t)sizeof(float)))
            { alt::setter(obj, ind, &dec, 5, 1); }
        }
    }
}

