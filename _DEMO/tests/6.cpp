
    const size_t first  = rand()%10;
    const size_t second = rand()%10;

    Cap obj = vms::capturer(first);

    printf("Allocation %s-successful.\n", vms::stable(obj)? "":"un"); fflush(stdout);

    printf("Requested size was %u, got %u.\n", first, vms::get_sz(obj)); fflush(stdout);

    bool grow, successfully;
    size_t change, from, to;

    for (size_t i=0; i<second; i++) {
        printf("\tTest %u of %u:\n", i+1, second); fflush(stdout);
        for (size_t j=0; j<second; j++) {
            grow = (bool) (rand()%7%2);
            change = rand()%10;
            ///
            from   = vms::get_sz(obj);
            if (grow) {successfully = vms::grow_cap(obj, change);}
            else      {successfully = vms::shrinker(obj, change);}
            to     = vms::get_sz(obj);
            ///
            printf("\t\tThe object %s-successfully %s %2u bytes from %2u to %2u.\n", successfully? "  ":"un", grow? "grew  ":"shrunk", change, from, to); fflush(stdout);
        }
    }

    vms::abort_cap(obj, false);
