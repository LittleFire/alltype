#include "main.hpp"

/*
    type scheme
    1 char
    2 char*
    3 int
    4 int*
    5 float
    6 float*
*/

void display(Morph);
void make(Morph, size_t);

int main(void) {
    printf("Test (non formal)\n\n"); fflush(stdout);
    srand(time(NULL));

    Coord asz = alt::createSpair((size_t)(1+rand()%10), false); Morph obj = alt::create(asz); free(asz); asz = NULL;
    if (alt::flag(obj, false)) {
        ///
        printf("Good, %i.\n", alt::length(obj)); fflush(stdout);
        for (size_t i=0; i<alt::length(obj); i++) {make(obj, i);}
        display(obj);
        ///
        {
            size_t srt = alt::length(obj);
            if (alt::resizer(obj, alt::length(obj)+(1+rand()%10))) {
                printf("Resized to %i elements.\n", alt::length(obj)); fflush(stdout);
                for (size_t i=srt; i<alt::length(obj); i++) {make(obj, i);}
                display(obj);
                ///
                if (alt::resizer(obj, srt)) {
                    printf("Shrunken back.\n"); fflush(stdout);
                    display(obj);
                }
            }
        }
        ///
        {
            printf("Manipulations\n"); fflush(stdout); size_t deleted = 0;
            for (size_t i=0; i<alt::length(obj); i++) {
                if (rand()%2) {
                    const bool successful = alt::deleter(obj, i);
                    printf("\tDeletion of element #%i was [%s]successful.\n", i+deleted, successful? "":"un");
                    fflush(stdout);
                    if (successful) {deleted++;}
                }
            }
            display(obj);
            for (size_t i=0; i<alt::length(obj); i++) {
                if (rand()%2) {
                    const bool successful = alt::inserter(obj, i);
                    printf("\tInsertion of element #%i was [%s]successful.\n", i, successful? "":"un");
                    fflush(stdout);
                    if (successful) {make(obj, i);}
                }
            }
            display(obj);
        }
        ///
    }
    alt::destroy(obj, false);

    printf("\n Done.\n"); fflush(stdout);
    #ifndef DEBUG
        getch(); fflush(stdin);
    #endif // DEBUG
    return 0;
}


void display(Morph obj) {
    if (alt::length(obj)==0) {
        printf("Empty");
        fflush(stdout);
    }
        printf("[%s]%c", alt::flag(obj, false)? "Good":"Bad", alt::length(obj)? '\n':' ');
        fflush(stdout);
    for (size_t x=0; x<alt::length(obj); x++) {
        printf("Element %i: ", x);
        switch (alt::typer(obj, x)) {
            case 0: {
                printf("NULL");
                break;
            }
            case 1: {
                printf("char %c" , *((char*)alt::getter(obj, x)));
                break;
            }
            case 2: {
                printf("char* %s",  ((char*)alt::getter(obj, x)));
                break;
            }
            case 3: {
                printf("int %d"  , *((int *)alt::getter(obj, x)));
            }
        }
        printf("\n");
        fflush(stdout);
    }
    putc('\n', stdout);
    fflush(stdout);
}

void make(Morph obj, size_t i) {
    switch (1+rand()%3) {
        case 1: {
            if (alt::resizer(obj, i, (size_t)sizeof(char))) {
                char c = (char) ('a'+rand()%26);
                alt::setter(obj, i, &c, 1, 1);
            }
            break;
        }
        case 2: {
            if (alt::resizer(obj, i, (size_t)sizeof(char))) {
                char c[5]; for (int z=0; z<4; z++) {c[z] = (char) ('a'+rand()%26);} c[4] = '\0';
                alt::setter(obj, i, c, 2, 5);
            }
            break;
        }
        case 3: {
            if (alt::resizer(obj, i, (size_t)sizeof(int ))) {
                int n = -100+rand()%201;
                alt::setter(obj, i, &n, 3, 1);
            }
        }
    }
}


