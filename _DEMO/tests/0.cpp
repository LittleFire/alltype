#include <cstring>
#include <cstdlib>
#include <cstdio>

typedef unsigned int sz_t;
inline sz_t _PTR_subtact(void* srt, void* end) {
    return (sz_t) end - (sz_t) srt;
}

int main(void) {
    printf("The sizes of an UnsignedInt, %u bytes, and a VoidPointer, %u, are%s equal.\n"
           "The size of a SizeT, %u bytes, is%s equal to an UnsignedInt and it's%s equal to a VoidPointer.\n\n",
            (sz_t) sizeof(sz_t), (sz_t) sizeof(void*), 
            sizeof(sz_t) == sizeof(void*)? "":" not",
            (sz_t) sizeof(size_t), 
            sizeof(sz_t) == sizeof(size_t)? "":"n't",
            sizeof(void*) == sizeof(size_t)? "":" not");
    fflush(stdout);
    
    char* str = "Hello world!";
    const size_t len=strlen(str);
    printf("%s\n", str); fflush(stdout);
    
    for (size_t i=0; i<len; i++) {
        if (i) printf(", ");
        printf("%u %c", i, str[i]);
    }
    printf("\n\n");
    fflush(stdout);
    
    size_t result;
    for (size_t i=0; i<len; i++) 
        for (size_t j=i; j<len; j++) {
            result = _PTR_subtact((void*) (str+i), (void*) (str+j));
            printf("%s %02u (%c) - %02u (%c) = %02u\n", 
                j-i==result? "True ":"False",
                (sz_t) j, str[j], 
                (sz_t) i, str[i],
                result);
            fflush(stdout);
        }
    
    return EXIT_SUCCESS;
}
