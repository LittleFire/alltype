#include <cstring>
#include <cstdlib>
#include <cstdio>

typedef unsigned int sizert;

bool _ptr_eq(void** ptr1, void** ptr2) {
    bool isequal = true; char* pnt1 = (char*)ptr1; char* pnt2 = (char*)ptr2;
    for(sizert i=0; i<(sizert)sizeof(void*); i++) {
        if(*(pnt1+i) != *(pnt2+i)) {
            isequal = false;
            break;
        }
    } return isequal;
}

bool _ptr_null(void** ptr) {
    bool nullequal = true; char* pnt = (char*)ptr;
    for(sizert i=0; i<(sizert)sizeof(void*); i++) {
        if(*(pnt+i) != '\0') {
            nullequal = false;
            break;
        }
    } return nullequal;
}

void* _ptr_render(void** ptr) {
    void* dest = NULL;
    for(sizert i=0; i<(sizert)sizeof(void*); i++) {
        memcpy(((char*)&dest)+i, ((char*)ptr)+i, sizeof(char));
    } return dest;
}

/*
void** _ptr_arithmetic(void** ptr, sizert cnt) {
    char* pnt = (char*)ptr; pnt += (sizert)sizeof(void*)*cnt;
    return (void**)pnt;
}
*/

void** _ptr_arithmetic(void** ptr, sizert cnt) {
    return (void**) (((char*)ptr) + (sizert)sizeof(void*)*cnt);
}

#define TYP int

int main(void) {
	TYP* a = (TYP*) malloc(sizeof(TYP));
	TYP* b = (TYP*) malloc(sizeof(TYP));
	TYP *c=a, *d=NULL, *f=NULL;

	printf("1 == %d %d\n",  _ptr_eq  ((void**)&a, (void**)&c), (char*)a == (char*)c);
	printf("2 == %d %d\n",  _ptr_eq  ((void**)&d, (void**)&f), (char*)d == (char*)f);
	printf("3 != %d %d\n", !_ptr_eq  ((void**)&a, (void**)&b), (char*)a != (char*)b);
	printf("4 Nl %d %d\n",  _ptr_null((void**)&a), a == NULL);
	printf("5 Nl %d %d\n",  _ptr_null((void**)&b), b == NULL);
	printf("6 Nl %d %d\n",  _ptr_null((void**)&d), d == NULL);
	printf("7 Nl %d %d\n",  _ptr_null((void**)&f), f == NULL);

    printf("\n");
        printf("8  a:Rr %p %p\n", a, _ptr_render((void**)&a));
        printf("9  b:Rr %p %p\n", b, _ptr_render((void**)&b));
        printf("10 c:Rr %p %p\n", c, _ptr_render((void**)&c));
        printf("11 d:Rr %p %p\n", d, _ptr_render((void**)&d));
        printf("12 f:Rr %p %p\n", f, _ptr_render((void**)&f));

    printf("\n");
	fflush(stdout);
	free(a); free(b);

	a = (TYP*) calloc(2, sizeof(TYP*));
	b = (TYP*) calloc(3, sizeof(TYP*));

	if(a != NULL and b != NULL) {
        TYP tmp = (TYP)'B'; memcpy(_ptr_arithmetic((void**)&a, 1), &tmp, sizeof(TYP));
            tmp = (TYP)'A'; memcpy(_ptr_arithmetic((void**)&a, 0), &tmp, sizeof(TYP));
            tmp = (TYP)'c'; memcpy(_ptr_arithmetic((void**)&b, 2), &tmp, sizeof(TYP));
            tmp = (TYP)'b'; memcpy(_ptr_arithmetic((void**)&b, 1), &tmp, sizeof(TYP));
            tmp = (TYP)'a'; memcpy(_ptr_arithmetic((void**)&b, 0), &tmp, sizeof(TYP));

        printf("A\n\t1 %d\n\t2 %d\n\t3 %d\n\t4 %d\n\t5 %d\n\n",
               *(TYP*)_ptr_arithmetic((void**)&a, 0),
               *(TYP*)_ptr_arithmetic((void**)&a, 1),
               *(TYP*)_ptr_arithmetic((void**)&b, 0),
               *(TYP*)_ptr_arithmetic((void**)&b, 1),
               *(TYP*)_ptr_arithmetic((void**)&b, 2));
        fflush(stdout);
	}

	free(a); free(b);
	return 0;
}

