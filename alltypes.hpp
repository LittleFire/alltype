#ifndef ALL_TYPES
#define ALL_TYPES

#ifndef __cplusplus
    #error Embed this library in a C++ program only.
#endif // __cplusplus

#include <cstdlib>

/// Data-types

struct AllType; typedef AllType* Morph; // (Treasure Planet)
struct Spair  ; typedef Spair  * Coord; // (Treasure Planet)
struct Svar   ; typedef Svar   * Chest; // (Treasure Planet)
struct Heap   ; typedef Heap   * HMan ; // Heap memory Emulation Adapter Processor
struct Host   ; typedef Host   * DPtr ; // DynamicPointer Tracking Record
struct Capture; typedef Capture* Cap  ; // allocation Capturer Adapter Precursor

enum class Erno { // Error number
    NO_ERROR,
    NULL_OBJECT,
    NULL_ARGUMENT,
    ZERO_ARGUMENT,
    ARGUMENT_TOO_HIGH,
    ARGUMENT_TOO_LOW,
    ARGUMENT_OUT_OF_BOUNDS,
    BOUNDS_ARGUMENT_OUT_OF_BOUNDS,
    BOUNDS_ARGUMENT_WRONG_ORDER
};

inline bool is_error(const Erno); // Check for NO_ERROR.

namespace alt {

    /// Sector A

    Morph create(size_t); // Create a container. (element_size)
    Morph create_free(size_t);
    Erno create_error(size_t);

    Morph create(Coord); // Create a container. (element_size, number_of_elements)
    Morph create_free(Coord);
    Erno create_error(Coord);

    bool recreate(Morph, size_t); // ReCreate a container. (element_size)
    bool recreate_free(Morph, size_t);
    Erno recreate_error(Morph, size_t);

    bool recreate(Morph, Coord); // ReCreate a container. (element_size, number_of_elements)
    bool recreate_free(Morph, Coord);
    Erno recreate_error(Morph, Coord);

    void destroy(Morph, bool); // UnCreate a container.
    void destroy_free(Morph, bool);
    Erno destroy_error(Morph, bool);

    bool copy_onto(Morph, Morph); // Copy a onto b.
    bool copy_onto_free(Morph, Morph);
    Erno copy_onto_error(Morph, Morph);

    Morph make_copy(Morph); // Copy a.
    Morph make_copy_free(Morph);
    Erno make_copy_error(Morph);

    bool flag(Morph, bool); // Retrieve the state of a flag in a container.
    bool flag_free(Morph, bool);
    Erno flag_error(Morph, bool);

    size_t length(Morph); // Retrieve the number of elements in a container.
    size_t length_free(Morph);
    Erno length_errors(Morph);

    bool resizer(Morph, size_t); // Change the number of elements in a container.
    bool resizer_free(Morph, size_t);
    Erno resizer_error(Morph, size_t);

    bool inserter(Morph, size_t); // Add an element to a container at an index.
    bool inserter_free(Morph, size_t);
    Erno inserter_error(Morph, size_t);

    bool deleter(Morph, size_t); // Remove an element from a container at an index.
    bool deleter_free(Morph, size_t);
    Erno deleter_error(Morph, size_t);

    bool is_index_null(Morph, size_t); // Check if an index of a container is ready for use.
    bool is_index_null_free(Morph, size_t);
    Erno is_index_null_error(Morph, size_t);

    size_t typer(Morph, size_t); // Retrieve the type of an element in a container.
    size_t typer_free(Morph, size_t);
    Erno typer_error(Morph, size_t);

    size_t sizes(Morph, size_t, bool); // Retrieve the size of an element in a container.
    size_t sizes_free(Morph, size_t, bool);
    Erno sizes_error(Morph, size_t, bool);

    bool resizer(Morph, size_t, size_t); // Change the size of an element in a container.
    bool resizer_free(Morph, size_t, size_t);
    Erno resizer_error(Morph, size_t, size_t);

    void* getter(Morph, size_t); // Retrieve a pointer to the stored data. (index)
    void* getter_free(Morph, size_t);
    Erno getter_error(Morph, size_t);

    void setter(Morph, size_t, void*, size_t, size_t); // Put data into the container. (index, source, type, size)
    void setter_free(Morph, size_t, void*, size_t, size_t);
    Erno setter_error(Morph, size_t, void*, size_t, size_t);

    /// Sector B

    Chest forge_new(size_t); // Create a vessel.
    Chest forge_new_free(size_t);
    Erno forge_new_error(size_t);

    bool forge(Chest, size_t); // ReCreate a vessel.
    bool forge_free(Chest, size_t);
    Erno forge_error(Chest, size_t);

    void dispose(Chest, bool); // UnCreate a vessel.
    void dispose_free(Chest, bool);
    Erno dispose_error(Chest, bool);

    bool replacer(Chest, Chest); // Copy a onto b.
    bool replacer_free(Chest, Chest);
    Erno replacer_error(Chest, Chest);

    Chest photocopy(Chest); // Copy a.
    Chest photocopy_free(Chest);
    Erno photocopy_error(Chest);

    bool ready(Chest); // Retrieve a vessel's good-flag.
    bool ready_free(Chest);
    Erno ready_error(Chest);

    bool rebase(Chest, size_t); // Change a vessel's base-size.
    bool rebase_free(Chest, size_t);
    Erno rebase_error(Chest, size_t);

    size_t detect(Chest); // Detect the data-type of a vessel's content.
    size_t detect_free(Chest);
    Erno detect_error(Chest);

    size_t dimensions(Chest, bool); // Retrieve the size of a vessel.
    size_t dimensions_free(Chest, bool);
    Erno dimensions_error(Chest, bool);

    void* receive(Chest); // Retrieve the data from within a vessel.
    void* receive_free(Chest);
    Erno receive_error(Chest);

    bool send(Chest, void*, size_t, size_t); // Put data into a vessel. (source, type, size)
    bool send_free(Chest, void*, size_t, size_t);
    Erno send_error(Chest, void*, size_t, size_t);

    /// Sector C

    Chest deposit_morph(Morph, size_t); // Convert a Morph object into a Chest object.
    Chest deposit_morph_free(Morph, size_t);
    Erno deposit_morph_error(Morph, size_t);

    Morph deposit_chest(Chest); // Convert a Chest object into a Morph object.
    Morph deposit_chest_free(Chest);
    Erno deposit_chest_error(Chest);

    /// Sector D

    Morph blank(); // Make a blank container for future building upon.
    Morph blank_free();
    Erno blank_error();

    Chest emptier(); // Make an empty vessel for future use.
    Chest emptier_free();
    Erno emptier_error();

    /// Sector E

    Coord createSpair(size_t, size_t); // Make a SPair object with a length and a depth.
    Coord createSpair_free(size_t, size_t);
    Erno createSpair_error(size_t, size_t);

    Coord createSpair(size_t, bool=true); // Make a SPair object with a single-value length and a given depth.
    Coord createSpair_free(size_t, bool=true);
    Erno createSpair_error(size_t, bool=true);

    size_t readSpair(Coord, bool); // Report on either a SPair object's length or it's depth.
    size_t readSpair_free(Coord, bool);
    Erno readSpair_error(Coord, bool);

    void editSpair(Coord, size_t, bool); // Change either a SPair object's length or it's depth.
    void editSpair_free(Coord, size_t, bool);
    Erno editSpair_error(Coord, size_t, bool);

    bool blankSpair(Coord); // Check whether a SPair object is blank (has any zeros).
    bool blankSpair_free(Coord);
    Erno blankSpair_error(Coord);

}

namespace vms { /// Virtual Memory management System

    /// Sector A

    HMan make_new(Cap); // Allocate a VMS.
    HMan make_new_free(Cap);
    Erno make_new_error(Cap);

    HMan make_new(size_t); // Allocate a VMS with a designated size.
    HMan make_new_free(size_t);
    Erno make_new_error(size_t);

    bool resizing(HMan, size_t); // Expand or shrink a VMS.
    bool resizing_free(HMan, size_t);
    Erno resizing_error(HMan, size_t);

    size_t compress(HMan, size_t, size_t, size_t, bool); // Eliminate unallocated bytes between allocated blocks. (min_byte_clear, startByte, endByte, direction)
    size_t compress_free(HMan, size_t, size_t, size_t, bool);
    Erno compress_error(HMan, size_t, size_t, size_t, bool);

    void release(HMan); // Deallocate a VMS.
    void release_free(HMan);
    Erno release_error(HMan);

    size_t get_size(HMan); // Report the number of bytes in the VMS.
    size_t get_size_free(HMan);
    Erno get_size_error(HMan);

    size_t get_used(HMan); // Report the number of used bytes in the VMS.
    size_t get_used_free(HMan);
    Erno get_used_error(HMan);

    size_t get_hsts(HMan); // Report the number of allocations tied to the VIB.
    size_t get_hsts_free(HMan);
    Erno get_hsts_error(HMan);

    bool sturdy(HMan); // Verify the successful allocation of a VMS.
    bool sturdy_free(HMan);
    Erno sturdy_error(HMan);

    DPtr issue_alloc(HMan, size_t, bool, size_t, size_t); // Request a piece of memory from a VMS. (size, parse_or_append, startHost, endHost)
    DPtr issue_alloc_free(HMan, size_t, bool, size_t, size_t);
    Erno issue_alloc_error(HMan, size_t, bool, size_t, size_t);

    DPtr reissue_alc(HMan, DPtr, size_t, bool); // Change the size of an allocation. (search_or_move, check_behind)
    DPtr reissue_alc_free(HMan, DPtr, size_t, bool);
    Erno reissue_alc_error(HMan, DPtr, size_t, bool);

    void free_alloct(HMan, DPtr); // Free an allocation from a VMS.
    void free_alloct_free(HMan, DPtr);
    Erno free_alloct_error(HMan, DPtr);

    bool change_index(HMan, DPtr, size_t); // Change the index of a DynamicPointer in a VMS.
    bool change_index_free(HMan, DPtr, size_t);
    Erno change_index_error(HMan, DPtr, size_t);

    size_t get_index(HMan, DPtr, bool); // Determine the index of a DynamicPointer in a VMS. (pointer, index_or_not)
    size_t get_index_free(HMan, DPtr, bool);
    Erno get_index_error(HMan, DPtr, bool);

    void* get_dptr(DPtr); // Get the static pointer from a DynamicPointer.
    void* get_dptr_free(DPtr);
    Erno get_dptr_error(DPtr);

    bool valid_ptr(DPtr); // Determine if a DynamicPointer is valid.
    bool valid_ptr_free(DPtr);
    Erno valid_ptr_error(DPtr);

    DPtr find_dptr(HMan, size_t); // Find a DynamicPointer on the Rails by index.
    DPtr find_dptr_free(HMan, size_t);
    Erno find_dptr_error(HMan, size_t);

    size_t index_get(HMan, DPtr); // Get the index of a DynamicPointer on the Rails.
    size_t index_get_free(HMan, DPtr);
    Erno index_get_error(HMan, DPtr);

    /// Sector B

    Cap capturer(size_t); // Allocate a Capturer. (size)
    Cap capturer_free(size_t);
    Erno capturer_error(size_t);

    void abort_cap(Cap, bool); // Free the contents of a Capturer. (pave_or_free)
    void abort_cap_free(Cap, bool);
    Erno abort_cap_error(Cap, bool);

    bool grow_cap(Cap, size_t); // Capture more memory (specified).
    bool grow_cap_free(Cap, size_t);
    Erno grow_cap_error(Cap, size_t);

    bool shrinker(Cap, size_t); // Release memory (specified).
    bool shrinker_free(Cap, size_t);
    Erno shrinker_error(Cap, size_t);

    bool stable(Cap); // Check if a Capturer was allocated successfully.
    bool stable_free(Cap);
    Erno stable_error(Cap);

    size_t get_sz(Cap); // Get the current size of a Capturer.
    size_t get_sz_free(Cap);
    Erno get_sz_error(Cap);

}

#endif
